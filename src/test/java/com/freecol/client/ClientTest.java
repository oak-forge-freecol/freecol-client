package com.freecol.client;
import com.freecol.client.FreeColClient;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Profile;
import org.springframework.test.context.ContextConfiguration;

@SpringBootTest
@Profile("test")
@ContextConfiguration(classes = FreeColClient.class)
class ClientTest {

    @Test
    public void someTest() { }

    @Test
    void test(){
        int a = 1;
        int b = 1;

        Assert.assertEquals(a,b);
    }

}