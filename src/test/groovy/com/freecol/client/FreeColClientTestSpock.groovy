package com.freecol.client

import org.springframework.context.annotation.Profile
import spock.lang.Specification

@Profile("test")
class FreeColClientTestSpock extends Specification {

    def "should be a simple assertion"() {
        expect:
        1 == 1
    }
}
