package com.freecol.client;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.boot.builder.*;
import org.springframework.context.*;

import javax.swing.*;
import java.awt.*;

import static java.lang.ClassLoader.*;


@SpringBootApplication
public class FreeColClient implements CommandLineRunner {


    public static void main(String[] args) {
        ApplicationContext context = new SpringApplicationBuilder(FreeColClient.class)
            .web(WebApplicationType.NONE)
            .headless(false)
            .bannerMode(Banner.Mode.OFF)
            .run(args);
    }

    @Override
    public void run(String... args) {
        EventQueue.invokeLater(() -> {
            JFrame frame = new JFrame();
            ImageIcon icon = new ImageIcon(getSystemResource("images/ship.jpg"));
            JLabel label = new JLabel(icon);
            frame.add(label);
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.pack();
            frame.setVisible(true);
        });
    }

}